extends Node2D

export (int) var speed = 80
export (int) var speed_random = 10
export (float) var rotation_speed = 3
export (float) var rotation_speed_random = 1

export (int) var sight_radius = 75

export (int) var separation_distance_too_close = 20
export (int) var alignment_average_heading_tolerance = 0.5
export (int) var obstacle_avoidance_tolerace = 50


# Owner of this boid, and presumably owner of all boids
var boid_owner = null

func _ready():
    # Not all boids are born the same
    speed = speed + rand_range(-1 * speed_random, speed_random)
    rotation_speed = rotation_speed + rand_range(-1 * rotation_speed_random, rotation_speed_random)

func set_owner(owner):
    boid_owner = owner

func _get_boid_list():
    return boid_owner.get_local_boids(self)

func _get_closest_boid(boid_list):
    var retval = {}
    retval["boid"] = null
    retval["distance"] = -1

    for boid in boid_list:
        var distance = boid.get_global_position().distance_squared_to(self.get_global_position())
        if retval["distance"] == -1 || distance < retval["distance"]:
            retval["boid"] = boid
            retval["distance"] = distance
    
    # Fix the distance!
    retval["distance"] = sqrt(retval["distance"])
    return retval

func _get_center_of_flock(boid_list):
    var average_x = 0
    var average_y = 0

    for boid in boid_list:
        average_x += boid.position.x
        average_y += boid.position.y
    
    average_x = average_x / boid_list.size()
    average_y = average_y / boid_list.size()

    return Vector2(average_x, average_y)

func _calculate_new_position(start_position, start_rotation, new_rotation, delta):
    var velocity = Vector2(speed, 0)
    velocity = velocity.rotated(new_rotation)
    velocity = velocity * delta

    return start_position + velocity

func _perform_movement(rotation_direction, delta):
    var new_rotation = self.rotation + (rotation_direction * rotation_speed * delta)    
    self.position = _calculate_new_position(self.position, self.rotation, new_rotation, delta)
    rotation = fmod(new_rotation, TAU)

func _process(delta):
    var rotation_dir = 0
    var boid_list = _get_boid_list()

    # Boids don't want to crash into walls, regardless of the rules
    var obstacle_result = _rule_of_obstacles(delta)
    if obstacle_result["honor"] == true:
        # We must avoid!
        if obstacle_result["turn"] == "cw":
            rotation_dir = 1
        else:
            rotation_dir = -1
        _perform_movement(rotation_dir, delta)
        return

    # Start following the rules of boids, assuming there are any nearby!
    if boid_list.size() == 0:
        _perform_movement(rotation_dir, delta)
        return

    # First up is separation
    var separate_result = _rule_of_separation(boid_list, delta)
    if separate_result["honor"] == true:
        # We must separate!
        if separate_result["turn"] == "cw":
            rotation_dir = 1
        else:
            rotation_dir = -1
        _perform_movement(rotation_dir, delta)
        return
    
    var alignment_result = _rule_of_alignment(boid_list)
    if alignment_result["honor"] == true:
        # We must align!
        if alignment_result["turn"] == "cw":
            rotation_dir = 1
        else:
            rotation_dir = -1
        _perform_movement(rotation_dir, delta)
        return
    
    var cohesion_result = _rule_of_cohesion(boid_list, delta)
    if cohesion_result["honor"] == true:
        # We must coheed!
        if cohesion_result["turn"] == "cw":
            rotation_dir = 1
        else:
            rotation_dir = -1
        _perform_movement(rotation_dir, delta)
        return
    
    # No rules need honoring! Move anyway
    _perform_movement(rotation_dir, delta)

func _rule_of_separation(boid_list, delta):
    var retval = {}
    retval["honor"] = false

    # Run through the boid list and make sure nothing is too close
    var closest_boid = _get_closest_boid(boid_list)
    if closest_boid["distance"] < separation_distance_too_close:
        # Oh no! Try to turn away!
        retval["honor"] = true
        # Do I need to turn left or right to avoid this boid?
        # We can simulate this by trying each direction and seeing which one gets us further away
        var rotation_0 = rotation + (0 * rotation_speed * delta)
        var new_pos_0 = _calculate_new_position(self.position, self.rotation, rotation_0, delta)

        var rotation_1 = rotation + (-1 * rotation_speed * delta)
        var new_pos_1 = _calculate_new_position(self.position, self.rotation, rotation_1, delta)

        var rotation_2 = rotation + (1 * rotation_speed * delta)
        var new_pos_2 = _calculate_new_position(self.position, self.rotation, rotation_2, delta)
        
        # Default to straight if it gets us further away
        var furthest = new_pos_0.distance_squared_to(closest_boid["boid"].position)

        retval["turn"] = "none"
        if new_pos_1.distance_squared_to(closest_boid["boid"].position) > furthest:
            retval["turn"] = "ccw"
            furthest = new_pos_1.distance_squared_to(closest_boid["boid"].position)
        
        if new_pos_2.distance_squared_to(closest_boid["boid"].position) > furthest:
            retval["turn"] = "cw"
            furthest = new_pos_2.distance_squared_to(closest_boid["boid"].position)

    return retval

func _rule_of_alignment(boid_list):
    var retval = {}
    retval["honor"] = false
    
    var average = 0
    for boid in boid_list:
        var delta = rotation - boid.rotation
        average += delta
        
    average = average / boid_list.size()

    if abs(average) > alignment_average_heading_tolerance:
        # Turn!
        retval["honor"] = true
        if average > 0:
            retval["turn"] = "ccw"
        else:
            retval["turn"] = "cw"

    return retval;

func _rule_of_cohesion(boid_list, delta):
    var retval = {}
    retval["honor"] = false

    # Get the "center of mass" for nearby boids
    var center_point = _get_center_of_flock(boid_list)
    # Do what it takes to get to this center point!
    
    # Do I need to turn left or right to avoid this boid?
    # We can simulate this by trying each direction and seeing which one gets us closer to the center
    var rotation_0 = rotation + (0 * rotation_speed * delta)
    var new_pos_0 = _calculate_new_position(self.position, self.rotation, rotation_0, delta)

    var rotation_1 = rotation + (-1 * rotation_speed * delta)
    var new_pos_1 = _calculate_new_position(self.position, self.rotation, rotation_1, delta)

    var rotation_2 = rotation + (1 * rotation_speed * delta)
    var new_pos_2 = _calculate_new_position(self.position, self.rotation, rotation_2, delta)
    
    # Default to straight if it gets us closer
    var nearest = new_pos_0.distance_squared_to(center_point)

    retval["turn"] = "none"
    if new_pos_1.distance_squared_to(center_point) < nearest:
        retval["honor"] = true
        retval["turn"] = "ccw"
        nearest = new_pos_1.distance_squared_to(center_point)
    
    if new_pos_2.distance_squared_to(center_point) < nearest:
        retval["honor"] = true
        retval["turn"] = "cw"
        nearest = new_pos_2.distance_squared_to(center_point)

    return retval

func _rule_of_obstacles(delta):
    var retval = {}
    retval["honor"] = false

    # If we continue on our current course will we get too close to a wall?
    var rotation_0 = rotation + (0 * rotation_speed * delta)
    var new_pos_0 = _calculate_new_position(self.position, self.rotation, rotation_0, delta)
    
    var future_x = new_pos_0.x
    var future_y = new_pos_0.y

    # Are we heading somewhere we don't like?
    if future_x - obstacle_avoidance_tolerace < 0 || future_x + obstacle_avoidance_tolerace > 1280 || future_y - obstacle_avoidance_tolerace < 0 || future_y + obstacle_avoidance_tolerace > 720:
        # Lets get as far away from that point as possible...
        retval["honor"] = true
        var rotation_1 = rotation + (-1 * rotation_speed * delta)
        var new_pos_1 = _calculate_new_position(self.position, self.rotation, rotation_1, delta)

        var rotation_2 = rotation + (1 * rotation_speed * delta)
        var new_pos_2 = _calculate_new_position(self.position, self.rotation, rotation_2, delta)

        # Attempt to get to the center of the arena
        var center_point = Vector2(640, 360)

        # Default to straight if it gets us closer
        var nearest = new_pos_1.distance_squared_to(center_point)
        retval["turn"] = "ccw"
        
        if new_pos_2.distance_squared_to(center_point) < nearest:
            retval["turn"] = "cw"
            nearest = new_pos_2.distance_squared_to(center_point)

        return retval
    
    # There's no reason to worry about obstalces for now
    return retval