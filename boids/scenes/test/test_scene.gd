extends Node2D

onready var boid_scene = load("res://objects/boid/boid.tscn")

export (int) var num_boids = 255

var boid_array = []

func _ready():
    # Spawn some amount of boids!
    _spawn_boids()

func _spawn_boids():
    boid_array = []
    var boids_spawned = 0

    # Spawn a new boid with random position and rotation
    while (boids_spawned < num_boids):
        var boid_instance = boid_scene.instance()
        boid_instance.position.x = rand_range(0, 1280)
        boid_instance.position.y = rand_range(0, 720)
        boid_instance.rotation_degrees = rand_range(0, 360)
        boid_instance.set_owner(self)
        
        add_child(boid_instance)
        boid_array.append(boid_instance)
        boids_spawned = boids_spawned + 1

func _process(delta):
    
    # The boids take care of themselves!
    pass

func get_local_boids(boid):
    var local_boids = []

    var sight_radius_squared = boid.sight_radius * boid.sight_radius

    for current_boid in boid_array:
        if current_boid != boid:
            # Is this boid close enough?
            if boid.position.distance_squared_to(current_boid.position) < sight_radius_squared:
                # Its local!
                local_boids.append(current_boid)
    
    return local_boids